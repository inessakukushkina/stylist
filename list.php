<!DOCTYPE html>
<html>
  <head>
    <title>Stylist</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="stylesheet" href="build/css/bootstrap.css">
    <link rel="stylesheet" href="build/css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,300" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
  </head>
  <body>
      <div class="container-fluid p-40">
        <h2 class="text-center">
            Stylist's list page
        </h2>
        <div class="row">
            <ul class="col-xs-12 m-t20">
                <?php               
                    $dir = opendir("build/"); 
                    if ($dir) {
                        while (false !== ($file = readdir($dir))) {
                            if ($file != "." && $file != ".." && $file != "css" && $file != "fonts" && $file != "favicon.ico" && $file != "js" && $file != "images") {
                                $file = preg_replace("/^(.*)\.[\w]{1,3}$/", "$1", $file); 

                                echo "
                                    <li class='col-md-3 col-sm-6 m-b20' style='display:inline-block; text-transform:capitalize'>
                                        <a href=/build/".$file.">".$file."</a>
                                    </li>
                                ";
                            }
                        }
                        closedir($dir); 
                    }
                ?>
            </ul>
        </div>
    </body>
</html>