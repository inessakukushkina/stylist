function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}

function bgImage($data_bg, $data_image){
    var _bg     = document.querySelectorAll($data_bg); 
    for (var i = 0; i < _bg.length; i++) {
        var _img    = _bg[i].querySelector($data_image);
        _bg[i].style.backgroundImage = 'url(' + _img.src + ')';
        _bg[i].removeChild(_img);
    }   
}

/*$.fn.quantityInput = function(){
    this.forEach(function(){
        var _this       = this;
        var decrement   = this.querySelector('[data-decrement]');
        var quantity    = this.querySelector('[data-control]');
        var incriment   = this.querySelector('[data-increment]');
        var _val        = quantity.getAttribute('value');
        if (parseInt($quantity.val()) <= 1) {
            $decrement.prop('disabled', true);
        } else {
            $decrement.prop('disabled', false);
        }
    });
}*/

CountGoods = {
    numericValidationString: /^[0-9]+$/
};

function ValidateNumeric(input){
    return CountGoods.numericValidationString.test(input);
}

var quantityInput = {

    /*count : {
        START: 1;
    }/*/

    START : 1,

    setDisabled: function(quantity, decrement){
        if(quantity === START){
            decrement.seatAttribute('disabled', true);
            quantity == START;
        } else{
            decrement.seatAttribute('disabled', false);
        }
    },

    setDecrement: function(quantity){
        setDisabled(quantity, descrment);
        if(ValidateNumeric(quantity)){
            quantity--;
            return (quantity <= START) ? START : quantity;
        } 
    },

    init: function(){
        var selectors = {
            quantity : documnent.querySelectorAll('quantity__control')
        }
    }
}

quantityInput.init();

$.fn.quantityInput = function(){
    $(this).each(function(){
    	var $_this 		= $(this),
        	$decrement  = $_this.find('[data-decrement]'),
            $quantity   = $_this.find('[data-control]'),
            $incriment  = $_this.find('[data-increment]');           

        if (parseInt($quantity.val()) <= 1) {
            $decrement.prop('disabled', true);
        } else {
            $decrement.prop('disabled', false);
        }

  		$decrement.on('click', function () {
            if (ValidateNumeric($quantity.val())) {
                $number = parseInt($quantity.val());
                $number--;
                if ($number <= 1) {
                    $quantity.val(1);
                	$decrement.prop('disabled', true);
                } else {
                	$quantity.val($number);
                	$decrement.prop('disabled', false);
                }                            
            } else {
            	$quantity.val(1);
             }
            return false;
        });
     	$incriment.on('click', function () {
            if (ValidateNumeric($quantity.val())) {
                $number = parseInt($quantity.val());
                $number++;

                $quantity.val($number);
                $decrement.prop('disabled', false);
            } else {
            	$quantity.val(1);
            }
            return false;
        });
    });
};

function CbChecked(formControl){
    for(var i=0; i < formControl.length; i++){
        formControl[i].addEventListener('change', function(){
            var show    = this.getAttribute('data-show');
            var hide    = this.getAttribute('data-hide');
            var all     = this.getAttribute('data-all');
            var toggle  = this.getAttribute('data-toggle');
            if (this.hasAttribute('data-show') || this.hasAttribute('data-hide')){
                if (this.checked){
                    addClass(document.querySelectorAll(hide)[0], 'is-hidden');
                    removeClass(document.querySelectorAll(show)[0], 'is-hidden')
                }
            }
            if (this.hasAttribute('data-all')){
                if (this.checked){
                    $(this).closest('table').find(formControl).prop('checked', true);
                } else{
                    $(this).closest('table').find(formControl).prop('checked', false);
                }
            }
            if (this.hasAttribute('data-toggle')){
                document.querySelectorAll(toggle)[0].classList.toggle('is-hidden');
            }
        });
    }
}