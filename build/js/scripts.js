$(document).ready(function() {
	bgImage('[data-bg]', '[data-img]');

	$('.slider').slick({
		slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
	})

	$('.quantity__block').quantityInput();

	var selectors = {
		"radio" 	: document.querySelectorAll('.cb-radiox'),
        "checkbo" 	: document.querySelectorAll('.cb-checkbox')
	}

	CbChecked(selectors.radio);
	CbChecked(selectors.checkbo);
})